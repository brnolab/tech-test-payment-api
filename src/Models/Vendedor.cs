using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace src.Models
{
    public class Vendedor
    {
        public int VendedorId { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public ICollection<Venda> Vendas { get; set; } // propriedade de navegação de coleção Livros para definir o relacionamento
    }
}