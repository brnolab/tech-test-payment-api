using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace src.Models
{
    public class Venda
    {
        public int VendaId { get; set; }
        public string NomeVendedor { get; set; }
        public DateTime Data { get; set; }
        public string ItensVendidos { get; set; }
        public int VendedorId { get; set; } //relacionado com VendedorId da entidade Vendedor
        public Vendedor Vendedor { get; set; } //propriedade de navegação de referencia
        public EnumStatus Status { get; set; }
    }
}