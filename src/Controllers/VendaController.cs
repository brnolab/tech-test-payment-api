using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using src.Context;
using src.Models;

namespace src.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        //Registrar venda: Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento";
        #region Registrar Venda
        
        [HttpPost]
        public IActionResult Registrar(Venda venda)
        {
            if (ModelState.IsValid)
            {
                _context.Vendas.Add(venda);
                _context.SaveChanges();
                return Ok();
            }
            return NotFound();
        }
        
        #endregion
        //Buscar venda: Busca pelo Id da venda;
        [HttpGet("{id}")]
        public IActionResult BuscaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if(venda == null)
            {
                return NotFound();
            }

            return Ok(venda);
        }
        //Atualizar venda: Permite que seja atualizado o status da venda.
        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if(vendaBanco == null)
            {
                return NotFound();
            }

            vendaBanco.Status = vendaBanco.Status;

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }

    }
}